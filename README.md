
Application uses `.htaccess` rules for routing.

### Apache

```apache
<VirtualHost *:80>
    # ...
    
    <Directory /var/www/html>
        Options FollowSymLinks
        AllowOverride All
        Order allow,deny
        allow from all
    </Directory>
</VirtualHost>
```

