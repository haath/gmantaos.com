<?php include("assets/php/utils.php"); ?>
<?php include("blog/php/config.php"); ?>

<?php

if (isset($_POST['password']))
{
	setcookie("auth", $_POST['password'], time() + (86400 * 30 * 30), "/"); // 1 month

}

if (auth())
{
	header('Location: /blog');
	exit();
}

?>

<html>
<head>
	<title>G. Mantaos</title>

	<?php include_header(); ?>

	<link rel="stylesheet" href="/assets/css/light.css"/>

	<style>

	#password
	{
		width: 12em;
		margin: auto;
	}

	</style>

</head>
<body class="text-center" cz-shortcut-listen="true">

	<?php include_navbar(); ?>

	<form method="POST">

		<input type="password" class="form-control" name="password" id="password">
		<br>

		<button type="submit" class="btn btn-primary">Submit</button>

	</form>

	<?php include_footer(); ?>

</body>
</html>