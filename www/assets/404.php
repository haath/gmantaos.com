<?php
    http_response_code(404);
?>
<html>
<head>
	<title>G. Mantaos</title>

	<?php include_header(); ?>

    <link rel="stylesheet" href="/assets/css/light.css"/>
    
    <style>
        .err-img
        {
            opacity: .8;
        }
    </style>
</head>
<body class="text-center" cz-shortcut-listen="true">

    <?php include_navbar(); ?>

    <img class="err-img" src="/assets/img/404.jpg">


    <br />
    <br />
    <br />

    <h4><a href="/blog/">&larr; Go Back</a></h4>


    <br />
    <br />
    <br />

    <?php include_footer(); ?>

</body>
</html>