<?php
  global $page_description, $page_keywords, $page_title, $page_image;

  $page_description = isset($page_description) ? $page_description : "G. Mantaos, Software Engineer";
  $page_title = isset($page_title) ? $page_title : "G. Mantaos - Software Engineer";
  $page_image = isset($page_image) ? $page_image : "/assets/img/favicon.png";
  $page_keywords = "blog,grigoris,greg,resume,mantaos,cv,computer,engineer,network," . $page_keywords;
?>

<meta name="description" content="<?= $page_description ?>" />
<meta name="keywords" content="<?= $page_keywords ?>" />

<!-- 
  Social media metadata
-->
<meta property="og:title" content="<?= $page_title ?>" />
<meta property="og:description" content="<?= $page_description ?>" />
<meta property="og:image" content="<?= "https://gmantaos.com" . $page_image ?>" />
<meta property="og:site_name" content="G. Mantaos" />
<meta property="og:type" content="profile" />

<meta name="twitter:image:alt" content="<?= $page_title ?>" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="<?= $page_title ?>" />
<meta name="twitter:description" content="<?= $page_description ?>" />
<!--
  End social media metadata
-->


<link rel="icon" type="image/png" href="/assets/img/favicon.png" />

<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Fonts -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Vollkorn" />

<!-- Bootstrap 4 -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://cdn.rawgit.com/afeld/bootstrap-toc/v1.0.0/dist/bootstrap-toc.min.css" />

<!-- Font Awesome 5 -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.0/css/all.css" rossorigin="anonymous" />

<!-- Custom CSS -->
<link rel="stylesheet" href="/assets/css/navbar.css" />
<link rel="stylesheet" href="/assets/css/theme.css" />
