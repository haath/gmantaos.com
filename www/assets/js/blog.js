const themes = {
	light: {
		style: '/assets/css/light.css',
		highlight: '/assets/css/highlight/github.min.css'
	},
	dark: {
		style: '/assets/css/dark.css',
		highlight: '/assets/css/highlight/vs2015.min.css'
	}
};

function toggleTheme(theme)
{
	$('#theme-link').attr('href', theme.style);
	$('#highlight-style').attr('href', theme.highlight);
}		

$(document).ready(function()
{
	/*
		Showdown convert
	*/
	var converter = new showdown.Converter({
		parseImgDimensions: true,
		tables: true,
		tasklists: true,
		ghMentions: true,
		ghCodeBlocks: true,
		backslashEscapesHTMLTags: true
	});

	
	var html = converter.makeHtml(text);
	$('#post-body').html(html);
	
	/*
		Highlight code
	*/
	$('pre code').each(function(i, block) {
		hljs.highlightBlock(block);
	});

	/*
		Add urls to header ids
	*/
	$('#post-body h1, #post-body h2').each(function(i, h)
	{
		$(this).prepend(
			$('<a class="header-link">').attr('href', '#' + this.id).html(
				$('<i class="fas fa-link">')
			)
		);
	});

	/*
		Add vertical rules under h1's
	*/
	$('#post-body h1').each(function(i, h)
	{
		$(this).after($('<hr>'));
	});

	/*
		Create TOC
	*/
	Toc.init({
		$nav: $('#post-toc'),
		$scope: $('#post-body')
	});
	$('#post-toc > ul.nav.navbar-nav')
	.append('<li><hr></li>')
	.append(
		$('<li class="toggle-container">')
		.append('<i class="fas fa-sun fa-lg fa-fw"></i>')
		.append(
			$('<label class="switch">')
			.append(
				$('<input id="theme-toggle" type="checkbox">').change(function()
				{
					if($(this).is(":checked"))
					{
						toggleTheme(themes.dark);
						document.cookie = "theme=dark";
					}
					else
					{
						toggleTheme(themes.light);
						document.cookie = "theme=light";
					}
				})
			)
			.append('<span class="slider round"></span>')
		)
		.append('<i class="fas fa-moon fa-lg fa-fw"></i>')
	)
	.append(
		$('<li>').append(
			$('<a class="nav-link" href="#">').html('&uarr; Back to top &uarr;').click(function()
			{
				$('html,body').animate({ scrollTop: 0 }, 'slow');
				$(this).blur();
			})
		)
	);

	/*
		Load the theme if specified in a cookie
	*/
	if (document.cookie.includes('theme=dark'))
	{
		$('#theme-toggle').prop('checked', true);
		toggleTheme(themes.dark);
	}

	/*
		Bind anchor links to smooth scrolling
	*/
	document.querySelectorAll('a[href^="#"]').forEach(anchor => {
		anchor.addEventListener('click', function (e) {
			e.preventDefault();

			history.pushState({}, '', this.getAttribute('href'));

			document.querySelector(this.getAttribute('href')).scrollIntoView({
				behavior: 'smooth'
			});
		});
	});

});