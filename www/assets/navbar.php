
<?php
	function is_active($page)
	{
		if (strpos($_SERVER["REQUEST_URI"], $page) !== false)
			return " active";
		else
			return "";
	}

	$back_url = preg_match("/^\/blog\/[A-Za-z0-9\-\_]+$/", $_SERVER["REQUEST_URI"]) ? "/blog/" : "/";
?>

<div class="container-fluid" id="header">
	<div class="row masthead">
		<div class="col-md-9 offset-xl-3 col-xl-3 nav-brand">
			<a href="<?= $back_url ?>">
				<img class="img-fluid masthead-brand float-left logo-img" src="/assets/img/logo.png">
			</a>
		</div>
		<div class="col-md-3 col-lg-3">
			<nav class="nav nav-masthead justify-content-center pull-right">
				<a class="nav-link<?= is_active("libraries") ?>" href="/libraries">Libraries</a>
				<a class="nav-link<?= is_active("games") ?>" href="/games">Games</a>
				<a class="nav-link<?= is_active("tools") ?>" href="/tools">Tools</a>
			</nav>
		</div>
	</div>
</div>