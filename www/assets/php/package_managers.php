<?php

$resp = file_get_contents("https://api-v2v3search-0.nuget.org/query?q=haath&prerelease=true");
$jresp = json_decode($resp);

$nuget_packages = array();

foreach ($jresp->data as $pkg)
{
	$nuget_packages[$pkg->id] = $pkg->totalDownloads;
}

function nuget_downloads($pkg_id)
{
	global $nuget_packages;
	return isset($nuget_packages[$pkg_id]) ? $nuget_packages[$pkg_id] : 0;
}

function crate_downloads($crate_id)
{
	$resp = file_get_contents("https://crates.io/api/v1/crates/" . $crate_id);
	$jresp = json_decode($resp);

	return $jresp->crate->downloads;
}

function github_downloads($project)
{	
	$opts = [
        'http' => [
                'method' => 'GET',
                'header' => [
                        'User-Agent: PHP'
                ]
        ]
	];

	$context = stream_context_create($opts);
	$resp = file_get_contents("https://api.github.com/repos/{$project}/releases", false, $context);
	
	$jresp = json_decode($resp);

	$sum = 0;

	foreach ($jresp as $release)
	{
		foreach ($release->assets as $asset)
		{
			$sum += $asset->download_count;
		}
	}

	return $sum;
}

?>