<?php

function include_header()
{
	include $_SERVER["DOCUMENT_ROOT"] . "/assets/header.php";
}
function include_navbar()
{
	include $_SERVER["DOCUMENT_ROOT"] . "/assets/navbar.php";
}
function include_footer()
{
	if (strpos($_SERVER["REQUEST_URI"], "libraries") !== false
	|| strpos($_SERVER["REQUEST_URI"], "games") !== false
	|| strpos($_SERVER["REQUEST_URI"], "tools") !== false)
	{
		include $_SERVER["DOCUMENT_ROOT"] . "/assets/footer-social.html";
	}

	include $_SERVER["DOCUMENT_ROOT"] . "/assets/footer.html";
}

function connect()
{
	global $cfg;
	return mysqli_connect($cfg['mysql_host'], $cfg['mysql_user'], $cfg['mysql_pass'], $cfg['mysql_db'], $cfg['mysql_port']);
}

function auth()
{
	global $cfg;
	return isset($_COOKIE['auth'])
		&& $_COOKIE['auth'] == $cfg['admin_pass'];
}

function title_url($title)
{
	$title = str_replace("'", "", $title);
	$title = str_replace("\"", "", $title);
	$title = str_replace(":", "", $title);
	$title = str_replace(".", "", $title);
	$title = str_replace(",", "", $title);
	$title = str_replace(" ", "-", $title);
	$title = str_replace("#", "sharp", $title);
	return urlencode($title);
}

?>