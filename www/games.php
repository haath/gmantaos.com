<?php include("assets/php/utils.php"); ?>
<?php include("assets/php/package_managers.php"); ?>
<html>
<head>
	<title>G. Mantaos | Games</title>

	<?php include_header(); ?>

	<link rel="stylesheet" href="/assets/css/light.css"/>
</head>
<body class="text-center" cz-shortcut-listen="true">

	<?php include_navbar(); ?>

	<div class="d-flex flex-column justify-content-center projects-container">

        <iframe src="https://itch.io/embed/525132?linkback=true&amp;border_width=5&amp;dark=true" width="560" height="175" frameborder="0"><a href="https://haath.itch.io/valchemy">Valchemy by Haath</a></iframe>

	</div>

    <?php include_footer(); ?>
</body>
</html>