<?php include("assets/php/utils.php"); ?>
<html>
<head>
	<title>G. Mantaos</title>

	<?php include_header(); ?>

	<link id="highlight-style" rel="stylesheet" href="/assets/css/highlight/vs2015.min.css">

	<link rel="stylesheet" href="/assets/css/dark.css"/>
</head>
<body class="text-center" cz-shortcut-listen="true">

	<?php include_navbar(); ?>

	<div class="d-flex flex-row justify-content-center my-flex-container">
		<div class="p-2 my-flex-item cont-column">

			<div class="float-right" style="text-align: right;">
				
				<img class="gravatar" src="https://secure.gravatar.com/avatar/658b0101347202e52b5cf690bcfa6c63?s=164">
				<br>
				<pre style="text-align: left;">
<code class="shell" id="shell">$ echo $GREETING</code>
</pre>

			</div>

		</div>
		<div class="p-2 my-flex-item"><div class="vertical-divider"></div></div>
		<div class="p-2 my-flex-item cont-column">
			
			<div class="lnk-list float-left">
				<ul class="nav flex-column">
					<li class="nav-item contact-item">
						<a class="nav-link" href="https://github.com/gmantaos"><i class="fab fa-fw fa-github"></i>&nbsp;Github</a>
					</li>
					<li class="nav-item contact-item">
						<a class="nav-link" href="https://gitlab.com/users/haath/projects"><i class="fab fa-fw fa-gitlab"></i>&nbsp;Gitlab</a>
					</li>
					<li class="nav-item contact-item">
						<a class="nav-link" href="https://www.linkedin.com/in/gmantaos/"><i class="fab fa-fw fa-linkedin"></i>&nbsp;LinkedIn</a>
					</li>
					<li class="nav-item contact-item">
						<a class="nav-link" href="https://haath.itch.io/"><i class="fab fa-itch-io"></i>&nbsp;itch.io</a>
					</li>
					<li class="nav-item contact-item">
						<a class="nav-link" href="mailto:gmantaos@protonmail.com" target="_top"><i class="fas fa-fw fa-envelope"></i>&nbsp;E-Mail</a>
					</li>
				</ul>
			</div>

		</div>
	</div>
	
<?php include_footer(); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/shell.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/cs.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/languages/ruby.min.js"></script>
<script>

	const script = [
		[ '\n.', 400 ],
		[ '.', 400 ],
		[ '.', 400 ],
		[ '.', 400 ],
		[ ' 404\n', 400 ]
	];

	$(document).ready(function()
	{
		hljs.highlightBlock($('#shell')[0]);
	});
	function append(line)
	{
		$('#shell').append(line);
		hljs.highlightBlock($('#shell')[0]);
	}
	var sum = 0;
	script.forEach(s => {
		setTimeout(_ => append(s[0]), s[1] + sum);
		sum += s[1];
	});
</script>
</body>
</html>
