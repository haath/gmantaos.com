<?php include("assets/php/utils.php"); ?>
<?php include("assets/php/package_managers.php"); ?>
<html>
<head>
	<title>G. Mantaos | Libraries</title>

	<?php include_header(); ?>

	<link rel="stylesheet" href="/assets/css/dark.css"/>
</head>
<body class="text-center" cz-shortcut-listen="true">

	<?php include_navbar(); ?>

	<div class="d-flex flex-column justify-content-center projects-container">

		<!--
			Haxe
		-->
		<h1 class="project-category">
			<a href="https://lib.haxe.org/u/Haath"
				title="lib.haxe.org"
				data-toggle="tooltip" data-placement="right">
				<img class="pkmg-icon" src="/assets/img/haxe.png"></a>
			Haxe
		</h1>
		<div class="projects-container">
			<a class="project-link" href="https://gitlab.com/haath/astar" target="_none">
				<div class="project">
					<div class="project-header">
						<img class="project-logo inverted" src="/assets/img/projects/astar.png">
					</div>
					<div class="project-content">
						<h3 class="project-title">astar</h3>
						<p>
							High-performance framework-agnostic 2D A-star solver.
						</p>
						<span class="float-right package-downloads">
						</span>
					</div>
				</div>
			</a>
			<a class="project-link" href="https://gitlab.com/haath/udprotean" target="_none">
				<div class="project">
					<div class="project-header">
						<img class="project-logo" src="/assets/img/projects/udprotean.png">
					</div>
					<div class="project-content">
						<h3 class="project-title">udprotean</h3>
						<p>
							Establishing virtual connections over UDP for reliable in-order message delivery.
							Utilizing UDP speed under a TCP-like handshake.
						</p>
						<span class="float-right package-downloads">
						</span>
					</div>
				</div>
			</a>
			<a class="project-link" href="https://gitlab.com/haath/bytesize" target="_none">
				<div class="project">
					<div class="project-header">
						<img class="project-logo inverted" src="/assets/img/projects/bytesize.png">
					</div>
					<div class="project-content">
						<h3 class="project-title">bytesize</h3>
						<p>
							Abstract type for human-readable byte sizes.
						</p>
						<span class="float-right package-downloads">
						</span>
					</div>
				</div>
			</a>
			<a class="project-link" href="https://gitlab.com/haath/quadtree" target="_none">
				<div class="project">
					<div class="project-header">
						<img class="project-logo inverted" src="/assets/img/projects/quadtree.png">
					</div>
					<div class="project-content">
						<h3 class="project-title">quadtree</h3>
						<p>
							Fast Quad-Tree 2D collision detection library, supporting all convex polygons.
						</p>
						<span class="float-right package-downloads">
						</span>
					</div>
				</div>
			</a>
		</div>
		<div class="projects-container">
			<a class="project-link" href="https://gitlab.com/haath/escript" target="_none">
				<div class="project">
					<div class="project-header">
						<img class="project-logo inverted" src="/assets/img/projects/escript.png">
					</div>
					<div class="project-content">
						<h3 class="project-title">escript</h3>
						<p>
							Simple macro-based event scripting engine for games.
						</p>
						<span class="float-right package-downloads">
						</span>
					</div>
				</div>
			</a>
			<a class="project-link" href="https://gitlab.com/haath/bytetype" target="_none">
				<div class="project">
					<div class="project-header">
						<img class="project-logo inverted" src="/assets/img/projects/bytetype.png">
					</div>
					<div class="project-content">
						<h3 class="project-title">bytetype</h3>
						<p>
							Zero-overhead byte array type comprehension through macros.
						</p>
						<span class="float-right package-downloads">
						</span>
					</div>
				</div>
			</a>
			<a class="project-link" href="https://gitlab.com/haath/pool" target="_none">
				<div class="project">
					<div class="project-header">
						<img class="project-logo inverted" src="/assets/img/projects/pool.png">
					</div>
					<div class="project-content">
						<h3 class="project-title">pool</h3>
						<p>
						 	Efficient macro-powered object pooling library.
						</p>
						<span class="float-right package-downloads">
						</span>
					</div>
				</div>
			</a>
			<a class="project-link" href="https://github.com/hxdefold/hxdefold" target="_none">
				<div class="project">
					<div class="project-header">
						<img class="project-logo inverted" src="/assets/img/projects/hxdefold.png">
					</div>
					<div class="project-content">
						<h3 class="project-title">hxdefold</h3>
						<p>
							The definitive library enabling the use of Haxe with the Defold game engine.
						</p>
						<span class="float-right package-downloads">
							(maintainer)
						</span>
					</div>
				</div>
			</a>
		</div>


		<div class="row">
			<div class="col-md-9">
				<!--
					.NET
				-->
				<h1 class="project-category">
					<a href="https://www.nuget.org/profiles/Haath"
						title="nuget.org"
						data-toggle="tooltip" data-placement="right">
						<img class="pkmg-icon" src="/assets/img/nuget.png"></a>
					.NET
				</h1>
				<div class="projects-container">
					<a class="project-link" href="https://github.com/gmantaos/Chance.NET" target="_none">
						<div class="project">
							<div class="project-header">
								<img class="project-logo" src="/assets/img/projects/chancenet.png">
							</div>
							<div class="project-content">
								<h3 class="project-title">Chance.NET</h3>
								<p>
									Originally ported from its Javascript equivalent, Chance.js,
									this library aims to provide painless integration with anything that needs legitimate and mathematically accurate
									random data.
								</p>
								<span class="float-right package-downloads">
									<?= nuget_downloads("Chance.NET") ?>&nbsp;<i class="fas fa-download"></i>
								</span>
							</div>
						</div>
					</a>
					<a class="project-link" href="https://github.com/gmantaos/Commander.NET" target="_none">
						<div class="project">
							<div class="project-header">
								<img class="project-logo" src="/assets/img/projects/commandernet.png">
							</div>
							<div class="project-content">
								<h3 class="project-title">Commander.NET</h3>
								<p>
									A pretty versatile library that allows parsing command-line arguments as custom serializable objects.
								</p>
								<span class="float-right package-downloads">
									<?= nuget_downloads("Commander.NET") ?>&nbsp;<i class="fas fa-download"></i>
								</span>
							</div>
						</div>
					</a>
					<a class="project-link" href="https://github.com/gmantaos/Quermine" target="_none">
						<div class="project">
							<div class="project-header">
								<img class="project-logo" src="/assets/img/projects/quermine.png">
							</div>
							<div class="project-content">
								<h3 class="project-title">Quermine</h3>
								<p>
									A significant abstraction over integrating any relational database into your
									.NET application, to the point of query-free interactions where your tables
									are merely classes in your code.
								</p>
								<span class="float-right package-downloads">
									<?= nuget_downloads("Quermine") ?>&nbsp;<i class="fas fa-download"></i>
								</span>
							</div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-md-3">
				<!--
					Rust
				-->
				<h1 class="project-category">
					<a href="https://crates.io/users/gmantaos"
						title="crates.io"
						data-toggle="tooltip" data-placement="right">
						<img class="pkmg-icon inverted" src="/assets/img/rust.png"></a>
					Rust
				</h1>
				<div class="projects-container">
					<a class="project-link" href="https://github.com/gmantaos/file-utils" target="_none">
						<div class="project">
							<div class="project-header">
								<img class="project-logo" src="/assets/img/projects/fileutils.png">
							</div>
							<div class="project-content">
								<h3 class="project-title">file-utils</h3>
								<p>
									A small crate, albeit a convenient one. Provides simple one-liners for efficiently reading and writing
									primitve types to IO streams.
								</p>
								<span class="float-right package-downloads">

								</span>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>

	</div>

<?php include_footer(); ?>
</body>
</html>