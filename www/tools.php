<?php include("assets/php/utils.php"); ?>
<?php include("assets/php/package_managers.php"); ?>
<html>
<head>
	<title>G. Mantaos | Libraries</title>

	<?php include_header(); ?>

	<link rel="stylesheet" href="/assets/css/dark.css"/>
</head>
<body class="text-center" cz-shortcut-listen="true">

	<?php include_navbar(); ?>

	<div class="d-flex flex-column justify-content-center projects-container">

		<div class="projects-container">
			<div class="row">
				<div class="col-md-6">

					<!--
						Go
					-->
					<h1 class="project-category">
						<img class="pkmg-icon" src="/assets/img/golang.png">
						Go
					</h1>
					<a class="project-link" href="https://github.com/gmantaos/Goirate" target="_none">
						<div class="project">
							<div class="project-header">
								<img class="project-logo inverted" src="/assets/img/projects/goirate.png">
							</div>
							<div class="project-content">
								<h3 class="project-title">Goirate</h3>
								<p>
									Intuitive CLI tool that intelligently scrapes the internet for safe and reliable torrents.
									Meant to act as a cron job scanner, looking out for new releases and automatically downloading them.
								</p>
								<span class="float-right package-downloads">
									<?= github_downloads("gmantaos/Goirate") ?>&nbsp;<i class="fas fa-download"></i>
								</span>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-6">

					<!--
						Ruby
					-->
					<h1 class="project-category">
						<a href="https://rubygems.org/profiles/Haath"
							title="crates.io"
							data-toggle="tooltip" data-placement="right">
							<img class="pkmg-icon" src="/assets/img/ruby.png"></a>
						Ruby
					</h1>
					<a class="project-link" href="https://github.com/gmantaos/yaml-cv" target="_none">
						<div class="project">
							<div class="project-header">
								<img class="project-logo inverted" src="/assets/img/projects/yaml-cv.png">
							</div>
							<div class="project-content">
								<h3 class="project-title">yaml-cv</h3>
								<p>
									Static generator of beautiful CVs from YAML. Exports into HTML and PDF formats.
								</p>
								<span class="float-right package-downloads">

								</span>
							</div>
						</div>
					</a>

				</div>
			</div>
		</div>

	</div>

<?php include_footer(); ?>
</body>
</html>
